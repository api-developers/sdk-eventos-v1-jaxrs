package com.cnova.seller.api.events.v1.model;

import java.util.Date;

import io.swagger.annotations.*;
import com.fasterxml.jackson.annotation.JsonProperty;


@ApiModel(description = "")
public class Event  {
  
  private Date eventDate = null;
  private Integer sellerId = null;
  private String eventType = null;
  private String resourceType = null;
  private Integer resourceId = null;
  private String uriResource = null;

  
  /**
   * Data e hora que o evento ocorreu
   **/
  @ApiModelProperty(required = true, value = "Data e hora que o evento ocorreu")
  @JsonProperty("eventDate")
  public Date getEventDate() {
    return eventDate;
  }
  public void setEventDate(Date eventDate) {
    this.eventDate = eventDate;
  }

  
  /**
   * Identificador do lojista
   **/
  @ApiModelProperty(required = true, value = "Identificador do lojista")
  @JsonProperty("sellerId")
  public Integer getSellerId() {
    return sellerId;
  }
  public void setSellerId(Integer sellerId) {
    this.sellerId = sellerId;
  }

  
  /**
   * Indica o motivo da geração do evento. Pode receber os valores 'NEW', 'APPROVED', 'CANCELED' ou 'CANCELED DENIED'
   **/
  @ApiModelProperty(required = true, value = "Indica o motivo da geração do evento. Pode receber os valores 'NEW', 'APPROVED', 'CANCELED' ou 'CANCELED DENIED'")
  @JsonProperty("eventType")
  public String getEventType() {
    return eventType;
  }
  public void setEventType(String eventType) {
    this.eventType = eventType;
  }

  
  /**
   * Indica o tipo de recurso que o evento se refere. Nesse caso é esperado apenas o valor 'Orders'
   **/
  @ApiModelProperty(required = true, value = "Indica o tipo de recurso que o evento se refere. Nesse caso é esperado apenas o valor 'Orders'")
  @JsonProperty("resourceType")
  public String getResourceType() {
    return resourceType;
  }
  public void setResourceType(String resourceType) {
    this.resourceType = resourceType;
  }

  
  /**
   * Identificador do recurso que gerou o evento
   **/
  @ApiModelProperty(required = true, value = "Identificador do recurso que gerou o evento")
  @JsonProperty("resourceId")
  public Integer getResourceId() {
    return resourceId;
  }
  public void setResourceId(Integer resourceId) {
    this.resourceId = resourceId;
  }

  
  /**
   * URI que poderá ser utilizada para consulta do recurso na API da Cnova.
   **/
  @ApiModelProperty(required = true, value = "URI que poderá ser utilizada para consulta do recurso na API da Cnova.")
  @JsonProperty("uriResource")
  public String getUriResource() {
    return uriResource;
  }
  public void setUriResource(String uriResource) {
    this.uriResource = uriResource;
  }

  

  @Override
  public String toString()  {
    StringBuilder sb = new StringBuilder();
    sb.append("class Event {\n");
    
    sb.append("  eventDate: ").append(eventDate).append("\n");
    sb.append("  sellerId: ").append(sellerId).append("\n");
    sb.append("  eventType: ").append(eventType).append("\n");
    sb.append("  resourceType: ").append(resourceType).append("\n");
    sb.append("  resourceId: ").append(resourceId).append("\n");
    sb.append("  uriResource: ").append(uriResource).append("\n");
    sb.append("}\n");
    return sb.toString();
  }
}
