package com.cnova.seller.api.events.v1.factories;

import com.cnova.seller.api.events.v1.EventsApiService;
import com.cnova.seller.api.events.v1.impl.EventsApiServiceImpl;

public class EventsApiServiceFactory {

    private final static EventsApiService service = new EventsApiServiceImpl();

    public static EventsApiService getEventsApi()
    {
        return service;
    }
}
