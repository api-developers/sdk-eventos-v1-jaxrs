package com.cnova.seller.api.events.v1;

import com.cnova.seller.api.events.v1.*;
import com.cnova.seller.api.events.v1.model.*;

import com.sun.jersey.multipart.FormDataParam;

import com.cnova.seller.api.events.v1.model.Event;

import java.util.List;
import com.cnova.seller.api.events.v1.NotFoundException;

import java.io.InputStream;

import com.sun.jersey.core.header.FormDataContentDisposition;
import com.sun.jersey.multipart.FormDataParam;

import javax.ws.rs.core.Response;

public abstract class EventsApiService {
  
    public abstract Response postEvent(Event event)
    throws NotFoundException;
  
}
