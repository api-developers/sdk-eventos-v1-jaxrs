package com.cnova.seller.api.events.v1;

import com.cnova.seller.api.events.v1.model.*;
import com.cnova.seller.api.events.v1.EventsApiService;
import com.cnova.seller.api.events.v1.factories.EventsApiServiceFactory;

import io.swagger.annotations.ApiParam;

import com.sun.jersey.multipart.FormDataParam;

import com.cnova.seller.api.events.v1.model.Event;

import java.util.List;
import com.cnova.seller.api.events.v1.NotFoundException;

import java.io.InputStream;

import com.sun.jersey.core.header.FormDataContentDisposition;
import com.sun.jersey.multipart.FormDataParam;

import javax.ws.rs.core.Response;
import javax.ws.rs.*;

@Path("/events")
@Consumes({ "application/json" })

@io.swagger.annotations.Api(value = "/events", description = "the events API")
public class EventsApi  {

    private final EventsApiService delegate = EventsApiServiceFactory.getEventsApi();
    
    @POST
    
    
    
    @io.swagger.annotations.ApiOperation(value = "", notes = "Cria um novo evento.", response = Void.class)
    @io.swagger.annotations.ApiResponses(value = { 
    @io.swagger.annotations.ApiResponse(code = 200, message = "OK") })
    public Response postEvent(@ApiParam(value = "Evento a ser enviado" ,required=true ) Event event)
    throws NotFoundException {
        return delegate.postEvent(event);
    }
}


