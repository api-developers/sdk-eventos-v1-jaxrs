# SDK Java JAX-RS - Eventos V1

Durante a integração com o Marketplace da Cnova, há momentos onde existe a necessidade de comunicação ou consulta de informações específicas situadas na plataforma do lojista.

![Fluxo da API do Lojista](http://cnovaportaldev.sensedia.com/api-portal/sites/default/files/images/sdk_serv.png)

A estrutura de código aqui apresentada tem o objetivo de fornecer uma implementação básica para construção do recurso de notificação de eventos ao lojista, podendo ser utilizada apenas como referência de implementação.

## Desenvolvimento

Os fontes desse projeto apenas expõe uma interface Restful no servidor onde o WAR for implantado.

Para integração com seu sistema, procure pela classe _com/cnova/seller/api/events/v1/impl/EventsApiServiceImpl.java_ e faça as implementações necessárias.

Um vez que o evento for recebido, a operação deverá retornar imediatamente com o código HTTP 200. Processamentos adicionais por parte do lojista, deverá ser feitos de forma que não segure a conexão entre o Marketplace e a API aberta por muito tempo.

### Exemplo de contrução de retorno

A resposta poderá ser efetuada da seguinte forma:

```java
return Response.ok().build();
```